(($) => {

  const mockEndpoint = 'https://run.mocky.io/v3/0feedea5-96d5-479b-a903-d9eec5c696c2';
  
  // TODO: Get images
  $('#image-picker-modal').on('shown.bs.modal', () => {
      $.get(mockEndpoint, (res) => {
        // res = { data: [{id: 1, image: 'image_url'}] }

        res.data.forEach(value => {
          $('#image-picker').append($('<option>', { value: value.id, 'data-img-src': value.image }));    
        })

        // Display image picker
        $("#image-picker").imagepicker();
      });
  })

  // TODO: Submit Selected Images
  $("#save").on("click", (e) => {
    e.preventDefault();

    const values = $('#image-picker').val();
    console.log(values);
    
    // Submit Image via Axios
    const payload = { values };

    $.post(mockEndpoint, payload, (res) => { 
      console.log(res);
    });
    
    // Close Modal
    $('#image-picker-modal').modal('toggle');
  });
  
  // TODO: When modal getting hidden
  $('#image-picker-modal').on('hidden.bs.modal', () => {
    // Reset on modal getting hidden
    $('#image-picker').empty();
    $('.image_picker_selector').empty();
  })
  
})(jQuery)